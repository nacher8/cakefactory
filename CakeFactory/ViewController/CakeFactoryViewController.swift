//
//  ViewController.swift
//  CakeFactory
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 5/12/21.
//

import UIKit
import SDWebImage

class CakeFactoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: - Constants
    private let BIRTHDAYCAKE: String = "Birthday cake"
    
    // MARK: - Properties
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var refreshInfoCake: UIButton!
    
    private var cakeList = [CakeModel]()
    
    private let url: String = "https://gist.githubusercontent.com/juananthony/c51c635c877d53d0fbc7d803f23af7ea/raw/0d4454a75859e8f94834a3de257b0b69a77e0b10/cakes"
    
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        self.getDataCake()
    }
    
    @objc private func getDataCake() {
        guard let url = URL(string: url) else {
                return
        }
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if error == nil {
                guard let cakeData = data else {
                    print("Nor data found")
                    return
                }
                
                do {
                    let decode = JSONDecoder()
                    self.cakeList = try decode.decode([CakeModel].self, from: cakeData).removingDuplicates().sorted { $0.title < $1.title }
                    print("Valores: \(self.cakeList)")
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            }
        }
        .resume()
    }
    
    // MARK: - UIButton Functions
    @IBAction func refreshInfoCake(_ sender: UIButton) {
        self.getDataCake()
    }
    
    // MARK: - CollectionView Functions
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cakeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CustomCollectionViewCell
        
        cell.cakeName.text = cakeList[indexPath.row].title
        cell.cakeImage.contentMode = .scaleAspectFit
        
        if cakeList[indexPath.row].title == BIRTHDAYCAKE {
            cell.cakeImage.image = UIImage(named: "birthdayCake")
        } else {
            cell.cakeImage.sd_setImage(with: URL(string: cakeList[indexPath.row].image))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cakeInfoVC = CakeFactoryDescriptionViewController(nibName: "CakeFactoryDescriptionViewController", bundle: nil)
        cakeInfoVC.cake = self.cakeList[indexPath.row]
        self.present(cakeInfoVC, animated: true)
    }
}

// MARK: - Extension Array
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
