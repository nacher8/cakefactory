//
//  CakeFactoryDescriptionViewController.swift
//  CakeFactory
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 8/12/21.
//

import UIKit

class CakeFactoryDescriptionViewController: UIViewController {
    // MARK: - Constants
    private let BIRTHDAYCAKE: String = "Birthday cake"
    
    // MARK: - Properties
    @IBOutlet weak var cakeImage: UIImageView!
    @IBOutlet weak var cakeName: UILabel!
    @IBOutlet weak var cakeDescription: UILabel!
    
    public var cake: CakeModel?
    
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        if cake?.title == BIRTHDAYCAKE {
            self.cakeImage.image = UIImage(named: "birthdayCake")
        } else {
            self.cakeImage.sd_setImage(with: URL(string: cake?.image ?? ""))
        }
        self.cakeName.text = cake?.title ?? ""
        self.cakeDescription.text = cake?.desc ?? ""
    }

}
