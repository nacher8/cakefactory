//
//  CustomCollectionViewCell.swift
//  CakeFactory
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 7/12/21.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    // MARK: - Properties
    @IBOutlet weak var cakeImage: UIImageView!
    @IBOutlet weak var cakeName: UILabel!
}
