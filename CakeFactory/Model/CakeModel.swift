//
//  CakeResultModel.swift
//  CakeFactory
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 6/12/21.
//

import Foundation

public struct CakeModel: Decodable, Hashable {
    var title: String
    var desc: String
    var image: String
}
